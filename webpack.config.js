const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
            'js': path.resolve('resources/js'),
            'utils': path.resolve('resources/js/utils'),
            'css': path.resolve('resources/css'),
            'components': path.resolve('resources/js/Components'),
            'layouts': path.resolve('resources/js/Layouts'),
            'images': path.resolve('public/images'),
            'data': path.resolve('public/data'),
        },
    },
};
