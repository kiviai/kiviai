<!DOCTYPE html>
<html lang="lt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/app-logo.svg">
        <link rel="stylesheet" href="/css/404.css">
        <title>Puslapis nerastas - 404</title>
    </head>
    <body class="body-404">
        <h1 class="text-404">Atsiprašome, tačiau šis puslapis neegzistuoja.</h1>
        <a href="/" class="link-404">Grįžti į namų puslapį</a>
    </body>
</html>
