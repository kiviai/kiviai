@component('mail::message')
Sveiki, **{{$name}}**,
po paskutinio Jūsų apsilankymo atsirado naujienų aoie Jūsų prenumeruojamas seniūnijas.
Jas galite pamatyti paspaudę žemiau:
@component('mail::button', ['url' => $link])
Peržiūrėti naujienas
@endcomponent

Pagarbiai,

„Pažink seniūnijas“ komanda
@endcomponent
