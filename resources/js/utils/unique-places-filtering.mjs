import FileSystem from 'fs';

const allPlaces = {
	
};

function execute() {
    const uniquePlaces = new Set();
    const filteredPlaces = {
        "amusement_park": [],
        "tourist_attraction": [],
        "aquarium": [],
        "zoo": [],
        "art_gallery": [],
        "museum": [],
        "city_hall": [],
        "church": [],
        "mosque": [],
        "synagogue": [],
        "park": [],
    };

    // Iterate over "tourist_attraction" at the end
    // because for example places like "museum" and "tourist_attraction" can overlap
    // so it would be better to keep "tourist_attraction" places as the last
    // priority only for places that do not fall into a different type.
    // Same with "museum"/"art_gallery" and so on.
    const typesPrioritised = [
        "city_hall",
        "aquarium",
        "zoo",
        "museum",
        "art_gallery",
        "amusement_park",
        "park",
        "mosque",
        "synagogue",
        "church",
        "tourist_attraction"
    ];

    let sameCount = 0;
    typesPrioritised.forEach((type) => {
        console.log("filtering:", type, ": Count before:", allPlaces[type].length);
        allPlaces[type].forEach(obj => {
            if (!uniquePlaces.has(obj.place_id)) {
                filteredPlaces[type].push(obj);
                uniquePlaces.add(obj.place_id);
            } else {
                sameCount++;
            }
        });
        console.log("Count after:", filteredPlaces[type].length);
    });

    console.log("Total places filtered out:", sameCount);

    FileSystem.writeFile(
        "filtered-places.json", 
        JSON.stringify(filteredPlaces),
        (error) => {
            if (error) {
                throw error;
            }
        }
    );
}

execute();
