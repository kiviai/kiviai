<?php

use App\Http\Controllers\LocationController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\rolesController;
use App\Http\Controllers\userController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\EventController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ProblemsController;
use App\Http\Controllers\BadgeController;
use App\Http\Controllers\ViktorinaController;
use App\Http\Controllers\VietosSpejimasController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// example of how to only let enter the page when logged in
// Route::get('dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

// ------------------------------------------------------- Admin
//TODO Reikia midlewaro sitam
Route::get('admin', function () {
    return Inertia::render('admin/index');
})->name('admin');

Route::get('problems', [ProblemsController::class, 'getProblemsPage'])->name('problems')->middleware('permission:quizErrorsResolve');
Route::post('submit-problem', [ProblemsController::class, 'createProblemEntry'])->name('submit-problem');
Route::post('submit-problemv', [ProblemsController::class, 'createProblemEntryV'])->name('submit-problemv');
Route::delete('delete-problem/{id}', [ProblemsController::class, 'destroy'])->name('delete-problem');
Route::post('update-problem/{id}',  [ProblemsController::class, 'updateProblem'])->name('update-problem');

Route::get('admin-news', function () {
    return Inertia::render('admin/News');
})->name('admin-news');

Route::controller(EventController::class)->group(function (){
    Route::get('admin-events', "aproval_index")->name('admin-events')->middleware('permission:eventAccept');
    Route::put('admin-events/{id}', "approve")->name('admin-events.approve')->middleware('permission:eventAccept');
});
Route::controller(NewsController::class)->group(function(){
   Route::get('/admin-news', 'admin_index')->name('admin-news')->middleware('permission:newsEditAll');
   Route::get('/admin-news/{id}','admin_show')->name('admin-single-news')->middleware('permission:newsEditAll');
});

// ------------------------------------------------------- Other
Route::get('/', function () {
    return Inertia::render('Home');
})->name('/');

Route::get('map', [MapController::class, 'index'])->name('map');

Route::get('games', function () {
    return Inertia::render('Games');
})->name('games');

Route::get('viktorina',[ViktorinaController::class, 'index'])->name('viktorina');

Route::get('vietosspejimas',[VietosSpejimasController::class, 'index'])->name('vietosspejimas');

Route::post('submit-vietosSpejimasZaidimas', [VietosSpejimasController::class, 'createVietosSpejimas'])->name('submit-vietosSpejimasZaidimas');


Route::get('routes', function () {
    return Inertia::render('Routes');
})->name('routes');

Route::get('routemap', function () {
    return Inertia::render('RouteMap');
})->name('routemap');

Route::get('activities', function () {
    return Inertia::render('Activities');
})->name('activities');

Route::get('badges', function () {
    return Inertia::render('Badges');
})->name('badges');

Route::get('seniunijos', function () {
    return Inertia::render('Seniunijos');
})->name('seniunijos');

Route::get('domeikava', function () {
    return Inertia::render('Seniunijos3/Domeikava');
})->name('Domeikava');

Route::get('samylu', function () {
    return Inertia::render('Seniunijos3/Samylu');
})->name('Samylu');

Route::get('traku', function () {
    return Inertia::render('Seniunijos3/Traku');
})->name('Traku');

Route::get('activities-add', function () {
    return Inertia::render('Activities-add');
})->name('activities-add');

Route::get('activities-my', function () {
    return Inertia::render('Activities-my');
})->name('activities-my');

Route::get('news/subscriptions', [NewsController::class, 'subscribedNews'])->name('news.subscriptions');
Route::post('news/subscriptions', [NewsController::class, 'manageSubscriptions']);
Route::resource('news', NewsController::class);

Route::get('problemos', function () {
    return Inertia::render('Problemos');
})->name('problemos');

Route::get('event/my', [EventController::class, 'myEvents'])->name('event.my');
Route::resource('event', EventController::class);

Route::resource('roles', rolesController::class);
Route::get('userRoles', [rolesController::class, 'userRoles'])->name('view-users-roles');
Route::put('userRoles', [rolesController::class, 'userRolesUpdate'])->name('update-users-roles');
Route::resource('problemsList', LocationController::class);

Route::get('zenkliukai', [BadgeController::class, 'index'])->name('zenkliukai');
Route::post('submit-zenkliukai', [ViktorinaController::class, 'addBadge'])->name('submit-zenkliukai');

Route::post('submit-viktorinosZaidimas', [ViktorinaController::class, 'createViktorina'])->name('submit-viktorinosZaidimas');

//Route::get('user', [userController::class, 'index'])->name('user-settings');
Route::get('user/{id}', [userController::class, 'index'])->name('user');



require __DIR__.'/auth.php';
