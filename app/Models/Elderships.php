<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Elderships
{
    private static $elderships = null;

    public static function getElderships()
    {
        if (self::$elderships == null)
            self::$elderships = DB::table('elderships')->orderBy('name')->pluck('name');

        return self::$elderships;
    }
}
