<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Badges;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, hasRoles;


// public function myEvents(){
//     return $this->hasMany('App\Event');
// }

    public function events(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Event');
    }

    public function hasBadge($badge_type){
        return $this->hasMany('\App\Models\Badges', 'userId')->where('badge_type', '=', $badge_type)->count();
    }


    public function getAdminAccessAttribute(){
        $permissions = [
            'userCreate',
            'userEditAll',
            'userDeleteAll',
            'userManageUserRoles',
            'userManageRoles',
        ];
        foreach ($permissions as $permission){
            if(!$this->can($permission))
                return false;
        }
        return true;
    }
    public function getManagerAccessAttribute(){
        $permissions = [
            'eventAccept',
            'quizErrors',
            'quizErrorsResolve',
        ];
        foreach ($permissions as $permission){
            if(!$this->can($permission))
                return false;
        }
        return true;
    }

    protected $appends = [
        'admin_access',
        'manager_access'
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',

        'roles'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
