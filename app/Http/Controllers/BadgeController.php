<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use App\Models\Badges;

class BadgeController extends Controller
{
    public function index()
    {
        $usersBadges = Badges::orderBy("id", "desc")->where('userId','=',Auth::id())->get();
        return Inertia::render('Badges', ['badgesu' => $usersBadges]);
    }

    public function createBadge(Request $request)
    {
        Validator::make($request->all(), [
            'badge_type' => ['required'],
        ])->validate();

        $badge = new badgeS;
        $badge->userId = $request->input('userId');
        $badge->badge_type = $request->input('badge_type');

        $badge->save();

        return redirect()->back()->with("message", "Sėkmingai pridėtas ženkliukas!")->with('messageType', 'approved');
    }
}
