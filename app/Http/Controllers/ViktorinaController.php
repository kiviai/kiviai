<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use App\Models\Viktorinos;
use App\Models\User;
use Illuminate\Support\Facades\Auth;;
use App\Models\Badges;

class ViktorinaController extends Controller
{
    public function index()
    {

        $games = User::join('viktorinos', 'users.id', '=', 'viktorinos.userId')->
            where('users.id', '=', Auth::id())->count();
        return Inertia::render('Viktorina', ['userGamesPlayed' => $games]);
    }

    public function createViktorina(Request $request)
    {
        if(Auth::user()){
        Validator::make($request->all(), [
            'userId' => ['required'],
        ])->validate();

        $viktorina = new viktorinos;
        $viktorina->userId = Auth::id();
        $viktorina->allTime = $request->input('allTime');
        $viktorina->average = $request->input('average');
        $viktorina->answeredQuestions = $request->input('answeredQuestions');
        $viktorina->skippedQuestions = $request->input('skippedQuestions');

        $viktorina->save();

        $gamesCount = User::join('viktorinos', 'users.id', '=', 'viktorinos.userId')->
        where('users.id', '=', Auth::id())->count();
        $user =  Auth::user();
        switch ($gamesCount){
            case 8:
                if(!$user->hasBadge("seniūnijų žinovas")) {
                    $badge = new badgeS;
                    $badge->userId = Auth::id();
                    $badge->badge_type = "seniūnijų žinovas";
                    $badge->save();
                    return redirect()->back()->with("message", "Sveikiname, gavote ženkliuką 'seniūnijų žinovas'")->with('messageType', 'approved');
                }
                break;
            case 14:
                if(!$user->hasBadge("seniūnijų ekspertas")) {
                    $badge = new badgeS;
                    $badge->userId = Auth::id();
                    $badge->badge_type = "seniūnijų ekspertas";
                    $badge->save();
                    return redirect()->back()->with("message", "Sveikiname, gavote ženkliuką 'seniūnijų ekspertas'")->with('messageType', 'approved');
                }
                break;
        }
        return redirect()->back();
        }
        return redirect()->back();
    }
}
