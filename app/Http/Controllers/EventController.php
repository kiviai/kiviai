<?php

namespace App\Http\Controllers;

use App\Models\Elderships;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Event;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Event::class, 'event');
    }

    public function index(Request $request)
    {
        $events = Event::orderBy('event_date', 'desc')->get()
            ->where('approval_status', '!=', 'Declined')->where('event_date', '>=', date("Y-m-d"))
            ->map(
            fn($event) => [
                'id' => $event->id,
                'title' => $event->title,
                'description' => $event->description,
                'events_categories' => $event->events_categories,
                'event_date'=> date("Y-m-d H:m",strtotime($event->event_date)),
                'districts' => $event->districts,
                'contact_email' => $event->contact_email,
                'editEvent'=> (Auth::user() != null) ? Auth::user()->can('update', $event) : false,
                'approval_status' => $event->approval_status,
                'longitude' => $event->longitude,
                'latitude' => $event->latitude,
            ]);
        $elderships = Elderships::getElderships();
        return Inertia::render('events/index', [
            'events' => $events,
            'elderships' => $elderships,
            'can' => [
                'createEvent' => (Auth::user() != null) ? Auth::user()->can('create', Event::class) : false,
            ]
        ]);
    }

    public function aproval_index(Request $request){

        $events = Event::all()->where('approval_status', '=', 'Pending')->map(
            fn($event) => [
                'id' => $event->id,
                'title' => $event->title,
                'description' => $event->description,
                'events_categories' => $event->events_categories,
                'districts' => $event->districts,
                'contact_email' => $event->contact_email,
                'approval_status' => $event->approval_status,
                'longitude' => $event->longitude,
                'latitude' => $event->latitude,
            ]);
        return Inertia::render('admin/Events', [
            'events' => $events,
        ]);
    }

    public function store(Request $request)
    {

            //TODO Enum validation required
            $validated = $request->validate([
                'title' => ['required', 'max:100'],
                'description' => ['required'],
                'event_date' => ['required'],
                'events_categories' => ['required'],
                'districts' => ['required'],
                'contact_email' => ['required', 'email'],
                'longitude' => ['required', 'numeric'],
                'latitude' => ['required', 'numeric'],
            ],
            [
                'title.required' => 'Irašykite pavadinimą',
                'title.max' => 'Pavadinimas negali būti ilgesnis nei 100 simbolių',
                'description.required' => 'Irašykite aprašymą',
                'event_date.required' => 'Pasirinkite datą',
                'events_categories.required' => 'Pasirinkite kategoriją',
                'districts.required' => 'Pasirinkite seniūniją',
                'contact_email.required' => 'Irašykite kontaktinio asments el. paštą',
                'contact_email.email' => 'Irašyti duomenys neatitinka el. pašto formato',
                'longitude.numeric' => 'Blogai pasirinktos koodrinatės, pasirinkite iš naujo',
                'longitude.required' => 'Žemėlapyje pasirinkite kur vyks renginys',
                'latitude.numeric' => 'Blogai pasirinktos koodrinatės, pasirinkite iš naujo',
                'latitude.required' => 'Žemėlapyje pasirinkite kur vyks renginys',
            ]);

            $data = $request->all();

            $data['event_date'] = date('Y-m-d H:i:s', strtotime($data['event_date']));
            $data['user_id'] = Auth::id();
            //TODO: Fix categories and status, kategoriju enum != kas svetaineje

            if(Auth::user()->can("eventCreateUnmanaged"))
                $data['approval_status'] = "Accepted";

        $event = Event::create($data);


            return redirect()->back()->with('message', 'Renginys sukurtas sėkmingai')->with('messageType', 'approved');
    }

    public function show(Event $event)
    {

        if (($event && ($event['approval_status'] == "Accepted") || (Auth::user() && Auth::id() == $event->user_id)))
            return Inertia::render('events/event', ['event' => $event]);
        return redirect()->route('event.index');
    }

    public function update(Request $request, Event $event)
    {
            Validator::make($request->all(), [
                'title' => ['required', 'max:100'],
                'description' => ['required'],
                'event_date' => ['required', 'date'],
                'events_categories' => ['required'],
                'districts' => ['required'],
                'contact_email' => ['required', 'email']
            ],
                [
                    'title.required' => 'Irašykite pavadinimą',
                    'title.max' => 'Pavadinimas negali būti ilgesnis nei 100 simbolių',
                    'description.required' => 'Irašykite aprašymą',
                    'event_date.required' => 'Pasirinkite datą',
                    'events_categories.required' => 'Pasirinkite kategoriją',
                    'districts.required' => 'Pasirinkite seniūniją',
                    'contact_email.required' => 'Irašykite kontaktinio asments el. paštą',
                    'contact_email.email' => 'Irašyti duomenys neatitinka el. pašto formato'
                ])->validate();

            if ($request->has('id') && $request->all()['id'] === $event->id) {
                $data = $request->all();
                $data['event_date'] = date('Y-m-d H:i:s', strtotime($data['event_date']));
                $event->update($data);
                return redirect()->back()->with('message', 'Renginys sėkmingai atnaujintas')->with('messageType', 'approved');
            }
            return redirect()->back()->with('messageError', 'Renginys nebuvo atnaujintas')->with('messageType', 'error');
    }

    public function destroy(Request $request, Event $event)
    {
            if ($request->has('id') && $request->all()['id'] === $event->id) {
                $event->delete();
                return redirect()->back()->with('message', 'Renginys pašalintas sėkmingai')->with('messageType', 'approved');
            }
            return redirect()->back()->with('message', 'Renginys nebuvo pašalintas')->with('messageType', 'error');
    }

    public function approve(Request $request, $id)
    {
        $event = Event::find($id);
        switch ($request->input('approval_status')) {
            case "Accepted":
                $event->update($request->all());
                return redirect()->back()->with('message', 'Renginys patvirtintas')->with('messageType', 'approved');
            break;
            default:
                $event->update($request->all());
                return redirect()->back()->with('message', 'Renginys Nepatvirtintas')->with('messageType', 'error');
        }
    }

    public function myEvents()
    {
//        $events = Event::where('user_id', Auth::id())->get();
        $events = Event::orderBy('event_date', 'desc')->get()->where('user_id', Auth::id());
        return Inertia::render('events/myEvents', [
            'events' => $events
        ]);
    }
}
