<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use App\Models\Location;

class LocationController extends Controller
{
    public function index()
    {
        $problemsList = Location::all();
        return Inertia::render('problemsList', ['problems' => $problemsList]);
    }
}
