<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use App\Models\problem;

class ProblemsController extends Controller
{
    public function getProblemsPage()
    {
        $problems = problem::orderBy("report_date", "desc")->get();
        return Inertia::render('admin/Problems', ['problems' => $problems]);
    }
    public function index()
    {
        $problemos = problem::get();
        return Inertia::render('map', ['problems' => $problemos]);
    }

    public function createProblemEntryV(Request $request)
    {
        Validator::make($request->all(), [
            'title' => ['required', 'max:100'],
            'description' =>  ['required', 'max:500'],
        ],
        [
            'title.required' => 'Įrašykite problemos pavadinimą',
            'description.required' => 'Įrašykite problemos aprašymaą',
            'title.max' => 'Problemos pavadinimas negali būti ilgesnis nei 100 simbolių',
            'description.max' => 'Problemos aprašymas negali būti ilgesnis nei 500 simbolių',
        ])->validate();

        $problem = new problem;
        $problem->title = $request->input('title');
        $problem->description = $request->input('description');
        $problem->problem_type = $request->input('problem_type');

        $problem->save();

        return redirect()->back()->with("message", "Sėkmingai pranešta apie klaidą!")->with('messageType', 'approved');
    }

    public function createProblemEntry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'max:100'],
            'description' =>  ['required', 'max:500'],
            'picture' => ['required', 'mimes:jpg,png,jpeg', 'max:2048'],
            'problem_type' => ['required'],
        ],
        [
            'title.required' => 'Įrašykite problemos pavadinimą',
            'problem_type.required' => 'Pasirinkite problemos tipą',
            'picture.required' => 'Ikelkite problemą apibūdinančią nuotrauką',
            'description.required' => 'Įrašykite problemos aprašymaą',
            'title.max' => 'Problemos pavadinimas negali būti ilgesnis nei 100 simbolių',
            'description.max' => 'Problemos aprašymas negali būti ilgesnis nei 500 simbolių',
            'picture.max' => 'Nuotrauka negali būti didesnė nei 2 megabaitai',
            'picture.mimes' => 'Blogas nuotraukos formatas, prašome pateikti jpg,png arba jpeg formatu',
        ])->validate();
        //TODO Padaryti, kad butu siunciamas error jei validacija failed
//        if ($validator->fails())
//        {
//            //$validator.messageError ="Nepavyko pranešti apie problemą, mėginkite dar kartą.";
//            return redirect()->back()->with($validator);
//        }

        $problem = new problem;
        $problem->title = $request->input('title');
        $problem->description = $request->input('description');
        $problem->problem_type = $request->input('problem_type');
        $problem->picture = $request->input('picture');

        $problem->latitude = $request->input('latitude');
        $problem->longitude = $request->input('longitude');

        $fileName = time().'.'.$request->picture->extension();
        $path = $request->picture->move(public_path('uploads'), $fileName);
        $problem->picture = $fileName;
        $problem->picture_path = $path;

        $problem->save();

        return redirect()->back()->with("message", "Sėkmingai pranešta apie problemą!")->with('messageType', 'approved');
    }


    public function updateProblem(Request $request)
    {

        $problems = problem::find($request->input('id'));

        if($problems) {
            $problems->status = $request->input('status');
            $problems->save();
        }
        return redirect()->back()->with("message", "Sėkmingai atnaujinta!")->with('messageType', 'approved');
    }

    public function destroy(Request $request)
    {
        if($request->has('id'))
        problem::find($request->input('id'))->delete();
        return redirect()->back()->with('message', 'Problema pašalinta sėkmingai')->with('messageType', 'approved');
    }

}
