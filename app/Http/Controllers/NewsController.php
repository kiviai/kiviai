<?php

namespace App\Http\Controllers;

use App\Mail\UnreadNewsMail;
use App\Models\Elderships;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function __construct()
    {
//        $this->authorizeResource(News::class, 'News');
    }

    private $validation = [
        'title' => ['required', 'min:10', 'max:100'],
        'content' => ['required'],
        'eldership_id' => ['required']
    ];
    private $messages = [
      "title.required" => "Irašykite naujienos pavadinimą",
      "content.required" => "Irašykite naujieną",
      "title.min" => "Naujienos pavadinimas turi būti bent 10 simbloių ilgio",
      "eldership_id.required" => "Pasirinkite seniūniją",
      "title.max" => "Naujienos pavadinimas turi būti ne ilgesnis nei 100 simbolių",
    ];

    public function index()
    {
        $data = News::join('elderships', 'elderships.id', '=', 'news.eldership_id')
            ->orderBy('news.date', 'desc')
            ->get(['news.*', 'elderships.name as eldership', 'elderships.id as elder_id'])
            ->map(
                fn($news) => [
                    'id' => $news->id,
                    'title' => $news->title,
                    'content' => $news->content,
                    'date' => date("Y-m-d",strtotime($news->date)),
                    'user_id' => $news->user_id,
                    'eldership' => $news->eldership,
                    'eldership_id' => $news->elder_id,
                    'editNews'=> (Auth::user() != null) ? Auth::user()->can('update', $news) : false,
                ]);
        $elderships = Elderships::getElderships();
        if(Auth::user() != null)
        $subscriptions = DB::table('subscriptions')
            ->where('user_id', Auth::id())
            ->pluck('eldership_id');
        else $subscriptions = false;
        return Inertia::render('news/index', [
            'newsL' => $data,
            'elderships' => $elderships,
            'subscriptions' => $subscriptions,
            'can' => [
                'createNews' => (Auth::user() != null) ? Auth::user()->can('create', News::class) : false,
            ]
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->validation, $this->messages);
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['date'] = date('Y-m-d', strtotime($data['date']));

        News::create($data);
//        $this->sendEmailsToSubscribers($data['eldership_id']);
        return redirect()->back()->with('message', 'Naujienos straipsnis sukurtas sėkmingai')->with('messageType', 'approved');
    }

    public function show($id)
    {
        $news =  News::join('elderships', 'elderships.id', '=', 'news.eldership_id')->
        get(['news.*', 'elderships.name as eldership'])->where('id', '=', $id)->first();
        if ($news !== null)
            return Inertia::render('news/news', ['news' => $news]);
        return redirect()->route('news.index');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));
        Validator::make($data, $this->validation)->validate();
        if ($request->has('id')) {
            News::find($request->input('id'))->update($data);
            return redirect()->back()->with('message', 'Naujienos straipsnis sėkmingai atnaujintas')->with('messageType', 'approved');
        }
        return redirect()->back()->with('message', 'Naujienos straipsnis nebuvo atnaujintas')->with('messageType', 'error');
    }

    public function destroy(Request $request)
    {
        if ($request->has('id'))
            News::find($request->input('id'))->delete();
        return redirect()->back()->with('message', 'Naujienos straipsnis pašalintas sėkmingai')->with('messageType', 'approved');
    }

    public function subscribedNews()
    {
        $data = News::join('elderships', 'elderships.id', '=', 'news.eldership_id')
            ->join('subscriptions', 'subscriptions.eldership_id', '=', 'news.eldership_id')
            ->orderBy('news.date', 'desc')
            ->where('subscriptions.user_id', Auth::id())
            ->get(['news.*', 'elderships.name as eldership'])->map(
                fn($news) => [
                    'id' => $news->id,
                    'title' => $news->title,
                    'content' => $news->content,
                    'date' => date("Y-m-d",strtotime($news->date)),
                    'user_id' => $news->user_id,
                    'eldership' => $news->eldership,
                    'eldership_id' => $news->elder_id,
                ]);
        $elderships = Elderships::getElderships();
        $subscriptions = DB::table('subscriptions')
            ->where('user_id', Auth::id())
            ->pluck('eldership_id');

        return Inertia::render('news/subscribedNews', [
            'newsL' => $data,
            'elderships' => $elderships,
            'subscriptions' => $subscriptions,
            'can' => [
                'createNews' => (Auth::user() != null) ? Auth::user()->can('create', News::class) : false,
            ]
        ]);
    }

    public function manageSubscriptions(Request $request)
    {
        DB::table('subscriptions')
            ->where('user_id', Auth::id())
            ->delete();

        $values = [];
        foreach ($request['elderships'] as $eldership)
            $values[] = ['user_id' => Auth::id(), 'eldership_id' => $eldership];

        DB::table('subscriptions')->insert($values);

        return redirect()->back()->with('message', 'Prenumeratos atnaujintos')->with('messageType', 'approved');
    }

    public function sendEmailsToSubscribers($eldership_id)
    {
        $subscribers = DB::table('subscriptions')
            ->join('users', 'users.id', '=', 'subscriptions.user_id')
            ->where('subscriptions.eldership_id', $eldership_id)
            ->pluck('users.name', 'users.email');
        $emailForm = new UnreadNewsMail();

        foreach($subscribers as $email => $name) {
            Mail::to($email)->send($emailForm->with([
                'name' => $name,
                'link' => 'localhost/news/subscriptions'
            ]));
        }
    }

    public function admin_index(){
        $data = News::join('elderships', 'elderships.id', '=', 'news.eldership_id')->join('users', 'users.id', '=' ,'news.user_id')->
        get(['news.*', 'elderships.name as eldership', 'users.name as user']);
        return Inertia::render('admin/news/index', [
            'news' => $data,
        ]);
    }
    public function admin_show($id)
    {
        $news =  News::join('elderships', 'elderships.id', '=', 'news.eldership_id')->join('users', 'users.id', '=' ,'news.user_id')->
        get(['news.*', 'elderships.name as eldership', 'users.name as user'])->where('id', '=', $id)->first();
        if ($news !== null)
            return Inertia::render('admin/news/News', ['news' => $news]);
        return redirect()->route('admin-news');
    }
}
