<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class rolesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:userManageRoles');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $roles = Role::all()->map(
            fn($role)=>[
                'id' =>$role->id,
                'name' =>$role->name,
                'permissions' => DB::table("role_has_permissions")
                    ->where("role_has_permissions.role_id",$role->id)
                    ->leftJoin('permissions','role_has_permissions.permission_id', '=', 'permissions.id')
                    ->pluck('permissions.name','permissions.id')
                    ->all()
            ]);
        $permissions = Permission::all()->map(
            fn($permission)=>[
                'id'=>$permission->id,
                'name'=>$permission->name
            ]);

        return Inertia::render('admin/roles/Roles',['roles'=>$roles, 'permissions'=>$permissions]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'max:100', 'unique:roles,name'],
            'perms' => ['required']
        ]);
        $data = $request->all();

        $role = Role::create(['name' => $data['name']]);
        foreach($data['perms'] as $permission )
            $role->givePermissionTo($permission);

        return redirect()->back()->with('message', 'Rolė sukurta sėkmingai')->with('messageType', 'approved');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Role $role)
    {
        $request->validate([
            'name' => ['required', 'max:100', Rule::unique('roles')->ignore($role->id)],
            'perms' => ['required']
        ]);

        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('perms'));

        return redirect()->back()->with('message', 'Rolė sėkmingai atnaujinta')->with('messageType', 'approved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return redirect()->back()->with('message', 'Rolė sėkmingai ištrinta')->with('messageType', 'approved');;
    }

    public function userRoles(){
        $roles = Role::where('name', '!=', 'Super Admin')->get();
        $users = User::all()->map(
            fn($user)=>[
                'id'=>$user->id,
                'name'=>$user->name,
                'email'=>$user->email,
                'roles'=>DB::table("model_has_roles")
                    ->where("model_has_roles.model_id",$user->id)
                    ->where("roles.name", "!=", "Super Admin")
                    ->leftJoin('users','model_has_roles.model_id', '=', 'users.id')
                    ->leftJoin('roles','model_has_roles.role_id', '=', 'roles.id')
                    ->pluck('roles.name', 'model_has_roles.role_id')
                    ->all()
            ]);
        return Inertia::render('admin/users/Users',['roles'=>$roles, 'users'=>$users]);
    }

    public function userRolesUpdate(Request $request){

        $data = $request->all();
        $user = User::where('id' , '=' , $data["id"])->get()[0];
        $user->SyncRoles($request->input('roles'));
        return redirect()->back()->with('message', 'Naudotojo '.$user->name.' rolės sėkmingai atnaujintos')->with('messageType', 'approved');
    }

}
