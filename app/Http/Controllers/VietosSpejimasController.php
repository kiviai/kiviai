<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use App\Models\VietosSpejimas;
use App\Models\User;
use Illuminate\Support\Facades\Auth;;
use App\Models\Badges;

class VietosSpejimasController extends Controller
{
    public function index()
    {

        $games2 = User::join('vietosspejimas', 'users.id', '=', 'vietosspejimas.userId')->
            where('users.id', '=', Auth::id())->count();
        return Inertia::render('VietosSpejimas', ['userGamesPlayed2' => $games2]);
    }

    public function createVietosSpejimas(Request $request)
    {
        if(Auth::user()){
        Validator::make($request->all(), [
            'userId' => ['required'],
        ])->validate();

        $vietosspejimass = new vietosspejimas;
        $vietosspejimass->userId = Auth::id();
        $vietosspejimass->points = $request->input('points');

        $vietosspejimass->save();

        $gamesCount = User::join('vietos_spejimas', 'users.id', '=', 'vietos_spejimas.userId')->
        where('users.id', '=', Auth::id())->count();
        $user =  Auth::user();

        switch ($gamesCount){
            case 6:
                if(!$user->hasBadge("nuotykių medžiotojas")) {
                    $badge = new badges;
                    $badge->userId = Auth::id();
                    $badge->badge_type = "nuotykių medžiotojas";
                    $badge->save();
                    return redirect()->back()->with("message", "Sveikiname, gavote ženkliuką 'nuotykių medžiotojas'")->with('messageType', 'approved');
                }
                break;
            case 3:
                if(!$user->hasBadge("įdomybių ieškotojas")) {
                    $badge = new badges;
                    $badge->userId = Auth::id();
                    $badge->badge_type = "įdomybių ieškotojas";
                    $badge->save();
                    return redirect()->back()->with("message", "Sveikiname, gavote ženkliuką 'įdomybių ieškotojas'")->with('messageType', 'approved');
                }
                break;
        }
        return redirect()->back();
        }
        return redirect()->back();
    }
}
