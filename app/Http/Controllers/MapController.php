<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\problem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class MapController extends Controller
{
    private static $places = null;
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        if (self::$places == null)
            self::$places = json_decode(file_get_contents("data/places.json"));

        $problems = problem::where('problem_type', '!=', 'viktorina')->get();
        $events = Event::where('approval_status', 'Accepted')->get();

        return Inertia::render('Map', [
            "places" => self::$places,
            "userProblems" => $problems,
            "events" => $events
        ]);
    }
}
