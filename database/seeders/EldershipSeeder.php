<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EldershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elderships=["Adutiškio","Agluonėnų","Akademijos","Akmenės","Antakalnio","Aukštadvario","Avižienių","Birštono","Daugų","Dieviškių","Domeikavos","Dusetų","Endriejavo","Gaižaičių","Gričiupio","Imbrado","Jašiūnų","Kačerginės","Kapčiamiesčio","Kernavės","Kybartų","Krekenavos","Liudvinavo","Luokės","Marcinkonių","Marijampolės","Merkinės","Meškuičių","Mindūnų","Mosėdžio","Nemenčinės","Neringos","Pagėgių","Pakalniškių","Pivašiūnų","Platelių","Punios","Rozalimo","Ruklos","Rumšiškių","Sasnavos","Senamiesčio","Stakliškių","Šakių","Šiluvos","Šunskų","Šveicarijos","Užliečių","Ventos","Vilkyškių","Židikų",];
        foreach ($elderships as $e)
            DB::table('elderships')->insert([
                'name' => $e
            ]);
    }
}
