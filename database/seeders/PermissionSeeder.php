<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @INFO: Before running this seeder, clear out the "Model_has_permissions",
     * "Model_has_roles", "Roles" and "role_has_permissions" table enteries
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        //

        $arrayOfPermissionNames = [
            'newsCreate', 'newsCreateUnmanaged', 'newsEditAll', 'newsDeleteAll',
            'userCreate', 'userEditAll', 'userDeleteAll', 'userManageUserRoles', 'userManageRoles',
            'eventCreate', 'eventCreateUnmanaged', 'eventEditAll', 'eventDeleteAll', 'eventAccept',
            'quizErrors', 'quizErrorsResolve',
        ];
        $permissions = collect($arrayOfPermissionNames)->map(function ($permission) {
            return ['name' => $permission, 'guard_name' => 'web'];
        });

        foreach($permissions as $permission)
            Permission::findOrCreate($permission['name']);


        //Creating system roles
        Role::findOrCreate('Super Admin');
        $role = Role::findOrCreate('Admin');
        $role->syncPermissions($arrayOfPermissionNames);
        $role = Role::findOrCreate('User');
        $role->syncPermissions(['eventCreate']);


//        $user = \App\Models\User::find(1);
//        $user->assignRole($role);
    }

}
