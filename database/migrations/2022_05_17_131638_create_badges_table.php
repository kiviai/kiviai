<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badges', function (Blueprint $table) {
            $table->id();
            $table->enum('badge_type', array('gamtos tyrėjas','gamtos mylėtojas','architektūros ieškotojas', 'architektūros atradėjas', 'renginių dalyvis', 'renginių gerbėjas', 'problemų pranešėjas', 'seniūnijų žinovas', 'seniūnijų ekspertas', 'įdomybių ieškotojas', 'nuotykių medžiotojas'));
            $table->bigInteger('userId')->unsigned();
            $table->foreign('userId')->references('id')->on('users');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badges');
    }
};




