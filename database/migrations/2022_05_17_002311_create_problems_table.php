<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('problems', function (Blueprint $table) {
            $table->enum('status', array('patvirtinta','nepatvirtinta','laukiama', 'išspręsta')) -> default('laukiama');
            $table->enum('problem_type', array('neasfaltuotas kelias','nenuvalytas kelias','neišvežtos šiukšlės', 'nenupjauta žolė', 'duobės kelyje', 'kita'));
            $table->timestamp('solved_date')->nullable();
            $table->float('longitude')->nullable();
            $table->float('latitude')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('problems', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('problem_type');
            $table->dropColumn('solved_date');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
        });
    }
};
