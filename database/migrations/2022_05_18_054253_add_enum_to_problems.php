<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE problems MODIFY COLUMN `problem_type` ENUM('neasfaltuotas kelias','nenuvalytas kelias','neišvežtos šiukšlės', 'nenupjauta žolė', 'duobės kelyje', 'kita', 'viktorina') NOT NULL DEFAULT 'kita'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE problems MODIFY COLUMN `problem_type` ENUM('neasfaltuotas kelias','nenuvalytas kelias','neišvežtos šiukšlės', 'nenupjauta žolė', 'duobės kelyje', 'kita') NOT NULL DEFAULT 'kita'");
    }
};
