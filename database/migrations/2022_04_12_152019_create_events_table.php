<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100);
            $table->longText('description');
            $table->timestamp('event_date');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->enum('events_categories', array("Koncertas","Minėjimas","Mugė","Dirbtuvės","Talka","Atlaidai","Kita",));
            $table->enum('districts', array('Adutiškio','Agluonėnų','Akademijos','Akmenės','Antakalnio','Aukštadvario','Avižienių','Birštono','Daugų','Dieviškių','Domeikavos','Dusetų','Endriejavo','Gaižaičių','Gričiupio','Imbrado','Jašiūnų','Kačerginės','Kapčiamiesčio','Kernavės','Kybartų','Krekenavos','Liudvinavo','Luokės','Marcinkonių','Marijampolės','Merkinės','Meškuičių','Mindūnų','Mosėdžio','Nemenčinės','Neringos','Pagėgių','Pakalniškių','Pivašiūnų','Platelių','Punios','Rozalimo','Ruklos','Rumšiškių','Sasnavos','Senamiesčio','Stakliškių','Šakių','Šiluvos','Šunskų','Šveicarijos','Užliečių','Ventos','Vilkyškių','Židikų'));
            $table->string('contact_email');
            //$table->forgein('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
