## About
PVP modulio K181 - Kiviai komandinis projektas
Developed with Laravel 9 + Vue 3

# Paleidimas / Išjungimas
Tam, kad paleisti projektą leidžiame šias komandas:
1. Einame į projekto root ir leidžiame `./vendor/bin/sail up` - Paleis docker konteinerį su visasi reikalingasi services.
2. Atsidarom naują terminalą ir einame į projekto root. Leidžiame `npm run watch` arba `npm run dev`. Tai subuildina visą js, css - frontą. Skirtumas tarp `watch` ir `dev` - tai, kad dev reikia leisti kas kartą norint pažiūrėti pakeitimus, o watch automatiškai buildina po kiekvieno failo saugojimo.

Tam, kad išjungti paleistą projektą, sustabdome docker konteinerį ir npm watch su `ctrl+c`. Tada einame į projekto root direktoriją ir leidžiame `./vendor/bin/sail down`.


# Installation

### Windows

Reikės:
- Docker
- WSL (Windows subsystem for linux)
- Your favourite IDE

### ALL

Pasipulinam projektą ir einam į vidų.

Pasidarom `.env.example` kopiją pavadinimu `.env`

Papildom `.env` failą eilute `MIX_API_KEY= čia įvesti API raktą`

instalinam composer dependencies `composer install`

leidžiame `php artisan key:generate`

Einame į projekto root. CLI leidžiame `./ventor/bin/sail up -d`

Atsidarom nauja CLI langa, Atsidarome docker konteinerį leidžiame `docker exec -it kiviai_laravel.test_1 bash`
Konteinerio viduje migruojam db `php artisan migrate`

Suinstalinam NPM dependencies `npm install`

Paleidziam, kad viska sukurtu `npm run dev`

---
Kai developinant pasileidziam `npm run watch`

